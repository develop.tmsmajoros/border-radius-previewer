---

# Border Radius Previewer

This is a small [Svelte](https://svelte.dev) apps. Original idea: https://github.com/florinpop17/app-ideas

[![Netlify Status](https://api.netlify.com/api/v1/badges/03d94fd5-749a-44a6-9d8b-459e78c55ff0/deploy-status)](https://app.netlify.com/sites/border-radius-preview/deploys)
